//
//  main.m
//  InitsDemo
//
//  Created by James Cash on 05-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Rectangle.h"
#import "Square.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Rectangle *r1 = [[Rectangle alloc] initWithHeight:5 width:7];
//        r1.width = 5;
        NSLog(@"Rect aspect is %f", r1.aspect);
//        r1.height = 7;
        // someobject.thing == [someobject thing]
        NSLog(@"Rect area is %f", r1.area);

        Square *sq1 = [[Square alloc] initWithHeight:5 width:7];
        sq1.width = 10;
        NSLog(@"square one = %f x %f = %f", sq1.width, sq1.height, sq1.area);

        Square *sq2 = [[Square alloc] initWithSize:4];
        sq2.height = 8;
        sq2.width = 7;
        NSLog(@"square two = %f x %f = %f", sq2.width, sq2.height, sq2.area);

    }
    return 0;
}
