//
//  Square.h
//  InitsDemo
//
//  Created by James Cash on 05-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Rectangle.h"

@interface Square : Rectangle

- (instancetype)initWithSize:(CGFloat)size;

@end
