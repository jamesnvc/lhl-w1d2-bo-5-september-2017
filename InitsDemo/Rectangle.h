//
//  Rectangle.h
//  InitsDemo
//
//  Created by James Cash on 05-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rectangle : NSObject /* {
  CGFloat _height; // ivar
}
- (CGFloat)height; // getter
- (void)setHeight:(CGFloat)newHeight; //setter
*/
@property (nonatomic,assign) CGFloat height;
@property (nonatomic,assign) CGFloat width;

- (instancetype)initWithHeight:(CGFloat)height
                         width:(CGFloat)width;

- (CGFloat)area;
- (CGFloat)aspect;

@end
