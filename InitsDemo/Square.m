//
//  Square.m
//  InitsDemo
//
//  Created by James Cash on 05-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Square.h"

@implementation Square

- (instancetype)initWithSize:(CGFloat)size
{
    self = [super initWithHeight:size width:size];
    return self;
}

- (instancetype)initWithHeight:(CGFloat)height width:(CGFloat)width
{
    self = [super initWithHeight:height width:height];
    return self;
}

- (void)setHeight:(CGFloat)height
{
    [super setHeight:height];
    [super setWidth:height];
}

- (void)setWidth:(CGFloat)width
{
    [super setWidth:width];
    [super setHeight:width];
}

@end
