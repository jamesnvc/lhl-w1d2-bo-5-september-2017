//
//  Rectangle.m
//  InitsDemo
//
//  Created by James Cash on 05-09-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Rectangle.h"

@implementation Rectangle

- (instancetype)initWithHeight:(CGFloat)height width:(CGFloat)width
{
    self = [super init];
    if (self) {
        _width = width;
        _height = height;
    }
    return self;
}

- (CGFloat)area
{
    return self.width * self.height;
}

- (CGFloat)aspect {
    return self.width / self.height;
}

//- (void)setWidth:(CGFloat)newWidth {
//    NSLog(@"Setting the width to %f", newWidth);
////    _width = newWidth;
////    self.width = newWidth; // [self setWidth:newWidth];
//}

@end
